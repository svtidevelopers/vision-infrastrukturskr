class: middle, center, inverse

# Vision Infrastructure
## .pink[&mdash; Infrastrukturskrå &mdash;]

---
class: middle, center
# Full and complete team ownership of their highly available services
--
<br>&ndash;.pink[<br>without ever needing to be called in outside of office hours]

???
* Daniel ville ha denna stor och rosa
* Men det täcker faktiskt ganska mycket av vad som visionen vill uttrycka
* Det innehåller:
  * Anvar och autonomi för teamen
  * Och den illehåller en väldigt pointerad definition av kvalité för infrastruktur
  * Hur får man till sådana

---
# Infrastructure knowledge and collaboration
--

* Even and high knowledge level across all teams

--

* Executing and maintaining infrastructure projects that span several teams

--

* Evaluating and introducing new infrastructure technologies from within teams

---
# High autonomy of every team
--

* Making it easy to do the right thing

--

  * Access to required knowledge and resources

--

  * Clear and visible best practices and tools

--

* A high quality common platform to build our standard use cases on

???
* Must even hold up for all our underlying infrastructure outside our direct control (network, servers)

---
# A high level of responsibility in every team
--

* A culture where every team takes care of their own technical debt

???
* Consciously avoiding to negatively impact other teams

--

* Full and complete ownership of our highly available services without ever needing to be called in outside of office hours

---
# A high level of accessibility
???
* Yes, that concerns us as well, think DNS, HTTPS
--

* even to small percentages of users

--

* while simultaneously weighing into that equation the security of everyone

---
class: middle, center, inverse
???
* Great, but:

---
class: middle, center, inverse
# How do we get there?

---
???
* Vi måste inte hitta på nåt nytt här
* Det finns ju redan nåt som andra har utvecklat fram

---
class: middle
# DevOps
???
* Jag vet vi säger ofta att vi gör DevOps
* Men gör vi det?
* Om definitionen är att det finns en SRE i varje team...
* Vi gör väl nåt av det
* Det passar oss utmärkt

--
 .pink[enables **quality** in autonomy]

???
* It's an alternative to a more process- and control-orientated organisation for achieving quality

---
class: middle
# DevOps
--
 .pink[is culture]

---
class: middle
# DevOps .pink[is automation]

---
class: middle
# DevOps .pink[is measurement]

---
class: middle
# DevOps .pink[is sharing]

---
class: middle, center
## It's called .pink[CAMS] = .pink[C]ulture .pink[A]utomation .pink[M]easurement .pink[S]haring

---
class: middle
# DevOps
???
* Låt oss gå tillbaka till denna:
--
 .pink[is culture and sharing]
???
* Alltså inte nåt som en roll eller ett team kan hålla på med ensam

---
class: middle
# DevOps .pink[needs to be embraced by **SVTi as a whole**]

???
* And not only by specific teams
* To yield the quality improvement we wan to achieve

---
class: center, middle, inverse
# So what's missing, what should we specifically work with?

---
class: center, middle
# **Infrastructure .yellow[Quality]**

???
* Och det betyder ju inte att något specifikt team ska fan skärpa sig

---
class: center, middle
# Definition of .yellow[quality] in infrastructure
--

# =
# Craftsmanship Identity

---
# Excellence in all technical infrastructure aspects

--
* **Resilience** and operational security

--
* **Security** in every step

--
* **Automation** of all the things

--
* High **monitoring and alerting** coverage

--
* **Scalability** and capacity planning

---
# Guiding behavioural values

--
* Increasing and spreading our **knowledge**

--
* **No fear** of change

???
By taking small steps, failing fast and learning from it

--
* **User focus** - internal as well as external

--
* High **transparency and simplicity**

--
* Making **pragmatic, prestigeless and holistic** decisions

???
For the whole of SVTi to move forward

--
* **Collaborating** with the other craftsmanship areas


---
class: middle, center, inverse
# Who are we?

---
class: middle, center

## You tell me
--
 .pink[&ndash; if you want to part of a group meeting regularly]

???
* Det är mer en lös grupp av folk som jag pratar med
* Det fanns inte så mycket intresse när vi har försökt sånt
* Prove me wrong!
--

## Hang out at `#infrastructure`

---

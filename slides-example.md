class: center, middle, inverse

# Heading
## .pink[&mdash; sub head &mdash;]

???
* Speaker notes

---
# Heading

```bash
code
```

---
# Heading

* Point 1

???
* Notes about point

--

* Point 2

---
class: inverse

# Heading

.fitWidth[.center[![ImageName](image.jpg)]]
.footnote[Source: https://example.com/]

---
# Heading

.left-column[Text<br>more]

--

.right-column[Text<br>more]

# Vision Infrastrukturskrå

Presentation written in remark.js

This is mybased on my [remark.js](https://remarkjs.com/) template

## Compatibility

Optimised for Firefox. If you want to run the presentation in Chrome, you need to remove the `sourceUrl` property and put the markdown code from `slides.md` directly into `presentation.html`, as described below in PDF-export

## PDF export

For any export to work:

* Use `presentation-print.html` (it already has the `sourceUrl` removed from the `remark.create` configuration)
* Put the `.md` code into the `presentation-print.html` file as shown below:

```html
<body>
  <textarea id="source">

# content of slides.md here

  </textarea>
  <script src="https://gnab.github.io/remark/downloads/remark-latest.min.js">
  </script>
  <script>
    var slideshow = remark.create();
  </script>
</body>
```

* Remove all intermediary slide steps `--` (regex including newlines: `\n--$`)
* Remove al speaker notes

### Chrome print export

The following CSS code in `presentation-print.html` fixes the Chrome print layout (Firefox does not work in any case):
```css
/* Print/export styles */
@page {
  size: 1210px 681px;
  margin: 0;
}

@media print {
    .remark-slide-scaler {
        width: 100% !important;
        height: 100% !important;
        transform: scale(1) !important;
        top: 0 !important;
        left: 0 !important;
    }
}
```

Then load the `presentation-print.html` in Chrome and use the browser print function.

### Decktape

Go into the folder with the presentation and run the following:
```bash
docker run --rm -v `pwd`:/slides astefanutti/decktape /slides/presentation-print.html presentation.pdf
```
